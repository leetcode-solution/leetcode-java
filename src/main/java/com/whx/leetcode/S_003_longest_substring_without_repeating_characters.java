package com.whx.leetcode;

/**
 * <p>
 *     无重复字符的最长子串
 * </p>
 * <p>
 *     给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
 *
 *  
 *
 * 示例 1:
 *
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 *
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * 示例 3:
 *
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
 *      请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
 * 示例 4:
 *
 * 输入: s = ""
 * 输出: 0
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-substring-without-repeating-characters
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * </p>
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 独立思考： 考虑在一次遍历中完成选择， 下标依次增加添加到current, 如果出现重复，则把current从重复处拆分为2，记录大者， 然后下标继续往后走知道末尾
 */
public class S_003_longest_substring_without_repeating_characters {

    /**
     * 独立思考的伪代码
     */
    public int lengthOfLongestSubstring(String str) {

        List<String> list = Arrays.asList(str.split(""));
        List<String> current = new ArrayList<>();
        List<String> max = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            current.add(list.get(i));
            // 判断新加入的字符是否在current已经存在
            if(hasRepeatChar(current)) {
                // 1. 把current 按照当前字符切分成左右2个list，对比max取最长的赋值给max
                max = getMaxList(max, current, list.get(i) );
                // 同时把切分的右边作为新的current
                current = newCurrent;
            }
        }
        // 输出max
        print(max)
        return 0;
    }

//    最佳实现
//
//    public int lengthOfLongestSubstring(String s) {
//        // 记录字符上一次出现的位置
//        int[] last = new int[128];
//        for(int i = 0; i < 128; i++) {
//            last[i] = -1;
//        }
//        int n = s.length();
//
//        int res = 0;
//        int start = 0; // 窗口开始位置
//        for(int i = 0; i < n; i++) {
//            int index = s.charAt(i);
//            start = Math.max(start, last[index] + 1);
//            res   = Math.max(res, i - start + 1);
//            last[index] = i;
//        }
//
//        return res;
//    }
}
// abcdefg e