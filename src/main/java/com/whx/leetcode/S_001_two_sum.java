package com.whx.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *     https://leetcode-cn.com/problems/two-sum/
 * </p>
 * <p>
 *     给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 的那 两个 整数，并返回它们的数组下标。
 *
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
 *
 * 你可以按任意顺序返回答案。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[0,1]
 * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 * 示例 2：
 *
 * 输入：nums = [3,2,4], target = 6
 * 输出：[1,2]
 * 示例 3：
 *
 * 输入：nums = [3,3], target = 6
 * 输出：[0,1]
 *  
 *
 * 提示：
 *
 * 2 <= nums.length <= 103
 * -109 <= nums[i] <= 109
 * -109 <= target <= 109
 * 只会存在一个有效答案
 *
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/two-sum
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * </p>
 */
public class S_001_two_sum {

    /**
     * 思考， 最简单直接的方法是两层for循环比较和。但是复杂度为 O(n*n)
     * <p>
     *     可以考虑使用一个map，在一次循环中保存差值，一边对比一边计算,把复杂度降低为 O(n)
     * </p>
     *
     */
    public static int[] sumTwo(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                return new int[]{map.get(nums[i]), i};
            } else {
                map.put(target - nums[i], i);
            }
        }
        return new int[]{-1, -1};
    }

    public static void main(String[] args) {
//        print(sumTwo(new int[]{2, 7, 11, 15}, 9));
//        print(sumTwo(new int[]{3, 2, 4}, 6));
        print(sumTwo(new int[]{3, 2, 4, 5}, 7));
    }

    public static void print(int[] arr) {
        System.out.println(String.format("[%d, %d]", arr[0], arr[1]));
    }

    /**
     * 额外思考： 如果一组输入有组结果，都要求输出呢， 例如{3, 2, 4, 5}， target=7 ,输出[0, 2] 和 [1, 3]
     *
     * <p>
     *     想复杂了， 直接把 60行的return改成打印输出即可，或者保存在一个外部变量中在一次性返回
     * </p>
     */
}

